# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def create_seed_data
  data = YAML.load File.read('./db/restaurants.yml')
  data += YAML.load File.read('./db/waimai.yml')

  data = data.map{|item| item.merge price: (1..10).to_a.sample}

  Restaurant.create(data)
end

create_seed_data
