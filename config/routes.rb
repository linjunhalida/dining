Dining::Application.routes.draw do
  root "restaurants#index"

  resources :restaurants do
    collection do
      get :search
    end
  end
end
