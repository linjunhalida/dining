server 'halida@localhost', roles: [:web, :db, :app]

set :repo_url, 'halida@localhost:/Users/halida/data/workspace/nag/'
set :branch, :master

set :deploy_to, '/Users/halida/data/workspace/temp/cap/nag'

set :rails_env, "production"

