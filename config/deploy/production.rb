server 'halida@ffuu.org', roles: [:web, :db, :app], port: 2222

set :repo_url, 'git@bitbucket.org:linjunhalida/nag.git'
set :branch, :master

set :deploy_to, '/home/halida/workspace/nag_production'

set :rails_env, "production"

