# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dining::Application.config.secret_key_base = '5f3a727b90da857918c9dd662338a62db24703ba3dff1710e37d408210708e13f66443805d49e49b2cb59808e3de1da4cf81b7100e80362f744a65eba5813391'
