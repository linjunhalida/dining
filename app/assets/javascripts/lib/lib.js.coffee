window.mylib =
    Controller: Spine.Controller

    now: ->
        new Date().getTime()

    alert: (opt)->
        console.log opt
        alert(opt.text)

    msg: (data)->
        $('.notifications .inner').notify(message: {text: data.text}, fadeOut: { enabled: true, delay: 3000}).show()

    refresh: ->
        location.reload()


String.prototype.trim = ->
	this.replace(/^\s+|\s+$/g, "")
String.prototype.to_camel = ->
	this.replace(/(\-[a-z])/g, ($1)-> $1.toUpperCase().replace('-',''))
String.prototype.to_dash = ->
	this.replace(/([A-Z])/g, ($1)-> "-"+$1.toLowerCase())
String.prototype.to_underscore = ->
	this.replace(/([A-Z])/g, ($1)-> "_"+$1.toLowerCase())
