# this module make run function in html by using reference other then embed js.
# place the code into html:
#
#     %input.run_function{'data-name' => 'xxx'}
#
# and use check_run_function(), it will run js function xxx.

window.run_function = (self)->
    name = self.data('name')
    func = mylib[name] || window[name]
    unless func
        console.log "error, cannot found function: ", name, self
        return
    func(self.data())

init_controller = (obj)->
    # prepare data
    data = obj.data()
    nd = {}
    nd[k.to_underscore()] = v for k, v of data
    nd.el = obj

    name = obj.data('controller')
    namespaces = name.split('.')

    controller = "#{namespaces.pop()}Controller"

    lib = mylib
    for n in namespaces
        lib = lib[n]

    new lib[controller](nd)

window.check_run_function = ($, obj)->
    self = $(obj)

    # obj could be a list, so need loop it
    for item in self
        item = $(item)

        # check item itself
        if item.hasClass('run_function')
            run_function(item)

        # check children
        for run in item.find('.run_function')
            run_function($(run))

        # controller
        if item.data('controller')
            init_controller(item)

        for run in item.find('[data-controller]')
            init_controller($(run))
