class RestaurantsController < ApplicationController

  def index
    @restaurants = Restaurant
      .order('price desc')
      .paginate(page: params[:page], per_page: 20)
  end

  def search
    @restaurants = Restaurant.search_by_name(params[:q])
  end
end
