class Restaurant < ActiveRecord::Base
  include Searchable

  mappings dynamic: 'false' do
    indexes :name, type: 'string', analyzer: 'mmseg_complex'
    indexes :price, type: 'integer'
  end

  def self.search_by_name(name)
    return [] if name.blank?
    self.search(
         min_score: 1,
         query: {match: {name: name}},
         sort: [
                  {price: {order: :asc}},
                  {_score: {order: :desc}},
                ],
         size: 30,
         )
  end
end
