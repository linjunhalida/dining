require 'elasticsearch/model'

module Searchable
  extend ActiveSupport::Concern

  def self.index_name
    Rails.application.class.parent_name.underscore
  end

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    index_name Searchable.index_name

    def self.search_by(type, query)
      return self.all if query.blank?
      self.search query: {match: {type => query}}
    end
  end

end

