INDEX_NAME = Searchable.index_name

def init
  idx = Restaurant.__elasticsearch__.client.indices
  idx.delete index: INDEX_NAME rescue nil
  idx.create(index: INDEX_NAME,
      body: {
               settings: Restaurant.settings.to_hash,
               mappings: Restaurant.mappings.to_hash,
             })

  Restaurant.import
end


init
