# -*- coding: utf-8 -*-
require 'nokogiri'
require 'open-uri'
require 'yaml'

def download(url)
  sleep 0.3
  puts "downloading: #{url}"
  retried = 0
  begin
    open(url, 'r', read_timeout: 5)
  rescue Net::ReadTimeout, Errno::ETIMEDOUT
    retried += 1
    unless retried >= 50
      puts "retring #{retried}"
      retry
    end
    raise
  end
end

def get_page_list(page)
  doc = Nokogiri::HTML(download("http://www.smartshanghai.com/listings/restaurants/?page=#{page}"))
  doc.css('#searchrow .searchrow-right .vtitle a').map do |a|
    a['href']
  end.uniq
end

def get_page(page)
  get_page_list(page).map do |link|
    get_restaurant link
  end.compact
end


def get_restaurant(link)
  doc = Nokogiri::HTML(download(link))

  begin # fail format return nil
    name = doc.css('.venue_maincol-cont .vtitle')[0].content.strip
    location = doc.css('#vp1.tabsviewport .venue-cont-left .vitem .item-big')[1].content.strip
    description = doc.css('#vp1.tabsviewport #eddes')[0].children[-1].content.strip

    {name: name, location: location, description: description}
  rescue
    nil
  end

end

def get_all
  result = []

  # pages 188
  (1..188).each do |page|
    puts ">>>> get page #{page}"
    result += get_page(page)
    File.write('data.yml', result.to_yaml)
  end
end



def get_waimai_page(id)
  doc = Nokogiri::HTML(download("http://waimaichaoren.com/restaurant/list/shanghai/#{id}/"))
  items = doc.css('ul.fl li.fl')
  items.map do |i|
    {
      name: i.css('a')[0].content,
      location: i.css('span')[0].content,
    }
  end
end

def get_all_waimai
  result = []

  (1..40).each do |page|
    puts ">>>> get page #{page}"
    result += get_waimai_page(page)
    File.write('data.yml', result.to_yaml)
  end
end

